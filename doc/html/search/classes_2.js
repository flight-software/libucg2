var searchData=
[
  ['ucg2_5ffuture_5ftimeout',['UCG2_FUTURE_TIMEOUT',['../structUCG2__FUTURE__TIMEOUT.html',1,'']]],
  ['ucg2_5fheader',['UCG2_HEADER',['../structUCG2__HEADER.html',1,'']]],
  ['ucg2_5finbound',['UCG2_INBOUND',['../structUCG2__INBOUND.html',1,'']]],
  ['ucg2_5finstance',['UCG2_INSTANCE',['../structUCG2__INSTANCE.html',1,'']]],
  ['ucg2_5fmessage',['UCG2_MESSAGE',['../structUCG2__MESSAGE.html',1,'']]],
  ['ucg2_5freg_5fcb_5fret_5ftype',['UCG2_REG_CB_RET_TYPE',['../structUCG2__REG__CB__RET__TYPE.html',1,'']]],
  ['ucg2_5fregister',['UCG2_REGISTER',['../structUCG2__REGISTER.html',1,'']]],
  ['ucg2_5frequest',['UCG2_REQUEST',['../structUCG2__REQUEST.html',1,'']]],
  ['ucg2_5fscripted_5fheader',['UCG2_SCRIPTED_HEADER',['../structUCG2__SCRIPTED__HEADER.html',1,'']]],
  ['ucg2_5fsubroutine',['UCG2_SUBROUTINE',['../structUCG2__SUBROUTINE.html',1,'']]],
  ['ucg2_5ftime',['UCG2_TIME',['../structUCG2__TIME.html',1,'']]]
];
