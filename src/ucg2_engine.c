/**
 * @file ucg2_engine.c
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief UCG 2 Top-level components usually accessed by the user.
 * @version 0.1
 * @date 2019-07-16
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_engine.h"

UCG2_GENERIC_TYPE UCG2_empty_value = {
        .value_size = 0,
    };

/**
 * @brief Get the type of a remote register.
 * 
 * @param inst UCG instance pointer.
 * @param target Target device address
 * @param subtarget Target device subaddress
 * @param reg Target register
 * @param fut Pointer to future where result should eventually be placed.
 * @return int Nonzero on error.
 */
int UCG2_GetRegisterType(UCG2_INSTANCE * inst,
                        uint8_t target, 
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    fut->value.value_size = 1; // The size of the response will always be 1 byte.
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)RTYP,
        .length = 1
    };
    uint8_t pl = reg;
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = &pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Read the contents of a remote register.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param reg target register ID
 * @param fut future to put the value in when it arrives
 * @return int nonzero on error
 */
int UCG2_ReadRegister(UCG2_INSTANCE * inst,
                        uint8_t target, 
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)RQRY,
        .length = 1
    };
    uint8_t pl = reg;
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = &pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Get the status of a remotely running subroutine.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param sr target subroutine ID
 * @param fut future to put the value in when it arrives
 * @return int nonzero on error
 */
int UCG2_GetSubroutineStatus(UCG2_INSTANCE * inst,
                            uint8_t target,
                            uint8_t subtarget,
                            uint8_t sr,
                            UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)SQST,
        .length = 1
    };
    uint8_t pl = sr;
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = &pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Get the return value of a remotely running subroutine.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param sr target subroutine ID
 * @param fut future to put the value in when it arrives
 * @return int nonzero on error
 */
int UCG2_GetSubroutineReturn(UCG2_INSTANCE * inst,
                                uint8_t target,
                                uint8_t subtarget,
                                uint8_t sr,
                                UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)SVAL,
        .length = 1
    };
    uint8_t pl = sr;
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = &pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Write a value to a remote register.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param reg target register ID
 * @param data data to write to the remote register
 * @param fut future to put the value in when it arrives
 * @return int nonzero on error
 */
int UCG2_WriteRegister(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_GENERIC_TYPE data,
                        UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    // The value of this is the response flag, so only one byte.
    fut->value.value_size = 1;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)RWRT,
        .length = data.value_size + 1
    };
    uint8_t pl[hdr.length];
    pl[0] = reg;
    memcpy((pl+1), data.value, data.value_size);
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Run a remote subroutine.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param sr target subroutine ID
 * @param arg argument to provide to the remote subroutine
 * @param fut future to put the value of the response flag in when it arrives.
 * @return int nonzero on error
 */
int UCG2_RunSubroutine(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t sr,
                        UCG2_GENERIC_TYPE arg,
                        UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    // The value of this is the response flag, so only one byte.
    fut->value.value_size = 1;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)SRUN,
        .length = arg.value_size + 1
    };
    uint8_t pl[hdr.length];
    pl[0] = sr;
    memcpy((pl+1), arg.value, arg.value_size);
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
/**
 * @brief Stop a remote subroutine.
 * 
 * @param inst UCG instance
 * @param target target device address
 * @param subtarget target device subaddress
 * @param sr target subroutine ID
 * @param fut future to put the value of the response flag in when it arrives.
 * @return int nonzero on error
 */
int UCG2_StopSubroutine(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t sr,
                        UCG2_FUTURE_TIMEOUT * fut)
{
    if (!inst || !fut) return -1;
    // Prepare the future
    fut->ex = false;
    fut->value.value = NULL;
    // Generate a message
    UCG2_HEADER hdr = {
        .target = target,
        .subtarget = subtarget,
        .source = inst->address,
        .op = (unsigned int)STOP,
        .length = 1
    };
    uint8_t pl = sr;
    UCG2_MESSAGE msg = {
        .hdr = hdr,
        .pl = &pl
    };
    // Send the message synchronously.
    UCG2NP_SendMessage(&msg);
    // Add the request for the asynchronous response.
    return UCG2_AddRequestFromMessage(inst->requestbuf, &msg, fut);
}
// Core engine functions
/**
 * @brief Add a request to the request table.
 * 
 * @param rqtbl pointer to the beginning of the request table.
 * @param rq request to add.
 * @return int 0 on sucessful addition. 1 if table is full.
 */
int UCG2_AddRequest(UCG2_REQUEST * rqtbl, UCG2_REQUEST rq)
{
    // Go through the table and find an empty cell
    for (int i=0; i<UCG2_REQUESTBUF_LEN; i++) {
        if (rqtbl[i].target == 0 && rqtbl[i].subtarget == 0 && rqtbl[i].op == NOP) {
            // This is an empty cell, so replace it with the one we are trying to write.
            rqtbl[i] = rq;
            return 0;
        }
    }
    // If we get here, there were no empty cells.
    return 1;
}
/**
 * @brief Generate a request from a request message, and add it to the request table.
 * 
 * The rules for which messages can have requests generated for them are given in
 * the documentation for UCG2_GenerateRequestFromMessage.
 * 
 * @param rqtbl pointer to first element of request table
 * @param msg pointer to message to generate request for
 * @param fut future to assign to the request
 * @return int 0 on success, nonzero on failure.
 */
int UCG2_AddRequestFromMessage(UCG2_REQUEST * rqtbl, UCG2_MESSAGE * msg, UCG2_FUTURE_TIMEOUT * fut)
{
    UCG2_REQUEST rq = UCG2_GenerateRequestFromMessage(msg, fut);
    return UCG2_AddRequest(rqtbl, rq);
}
/**
 * @brief Remove a matching request from the request table.
 * 
 * Not all fields are matched, only the fields that make sense to match
 * a request to a response.  Namely, sent_time and fut are not matched.
 * 
 * @param rqtbl pointer to the beginning of the request table
 * @param rq_match matching request
 * @return int 0 on success, 1 if not found, -1 if some other error.
 */
int UCG2_RemoveRequest(UCG2_REQUEST * rqtbl, UCG2_REQUEST rq_match)
{
    UCG2_REQUEST empty = {
        .target = 0,
        .subtarget = 0,
        .op = NOP
    };
    for (int i=0; i<UCG2_REQUESTBUF_LEN; i++) {
        if (rqtbl[i].target == rq_match.target &&
            rqtbl[i].subtarget == rq_match.subtarget &&
            rqtbl[i].op == rq_match.op &&
            rqtbl[i].target_id == rq_match.target_id) 
        {
            // Request matches, so clear it.
            rqtbl[i] = empty;
            return 0;
        }
    }
    return 1;
}
/**
 * @brief From a request message, generate a request that corresponds to it.
 * This function should only be used to generate requests for operations.
 * Attempting to make a request for a flag will return an empty request.
 * If msg or fut are NULL, will also return empty request.
 * @param msg message to generate the request from.
 * @param fut future to assign to the request.
 * @return UCG2_REQUEST generated request.
 */
UCG2_REQUEST UCG2_GenerateRequestFromMessage(UCG2_MESSAGE * msg, UCG2_FUTURE_TIMEOUT * fut)
{
    UCG2_REQUEST empty = {
        .target = 0,
        .subtarget = 0,
        .op = NOP
    };
    if (!msg || !fut) return empty;
    // Check to see if this operation is actually something that needs a request.
    if (msg->hdr.op > 0xB) 
    {
        return empty;
    }
    // Configure everything that's not dynamic.
    UCG2_REQUEST rq = {
        .target = msg->hdr.target,
        .subtarget = msg->hdr.subtarget,
        .op = msg->hdr.op,
        .sent_time = UCG2NP_GetAbsTime(),
        .fut = fut,
    };
    // The target register/subroutine is in data[0] if it exists.
    if (msg->hdr.length > 0) {
        rq.target_id = msg->pl[0];
    } else {
        rq.target_id = 0;
    }
    return rq;
}
/**
 * @brief Generate a request that matches the inverse of a message.
 * 
 * This is used to generate the request that the message is a response to,
 * so that we can match responses to requests.  This function also handles
 * flags, assuming that directed flags are in response to the most recent 
 * operation.
 * 
 * @param rqtbl request table, used to generate matches for flags.
 * @param msg message to interpret.
 * @return UCG2_REQUEST 
 */
UCG2_REQUEST UCG2_GenerateRequestMatch(UCG2_REQUEST * rqtbl, UCG2_MESSAGE * msg)
{
    UCG2_REQUEST empty = {
        .target = 0,
        .subtarget = 0,
        .op = NOP
    };
    // Build a constant request that is the inverse of the response received,
    // i.e. the original request that we sent on our end.
    UCG2_REQUEST rq = {
        .target = msg->hdr.source,
        .subtarget = msg->hdr.source_sub
    };
    // Figure out the target ID, if applicable.
    if (msg->hdr.length > 0) {
        rq.target_id = msg->pl[0];
    } else {
        rq.target_id = 0;
    }
    /**
     * If this is a response message, like an RVAL, then generating the inverse,
     * in this case an RQRY, is relatively easy, and doesn't require us to look
     * up things in rqtbl.
     * However, a flag does not contain enough information (in general) to
     * enable us to easily match it to a specific message, so a lookup is
     * necessary.  We assume it comes from the oldest applicable message.
     */
    if (msg->hdr.op < 0xD) {
        /* This branch deals with operations.  
           Anything invalid will become an empty request. */
        switch (msg->hdr.op) {
            case RVAL:
                rq.op = RQRY;
            break;
            case RRTC:
                rq.op = RTYP;
            break;
            case STAT:
                rq.op = SQST;
            break;
            case SRET:
                rq.op = SVAL;
            break;
            default:
                return empty;
            break;
        }
    } else {
        /* This branch deals with flags.  This requires
        looking up what the oldest applicable request is,
        and then matching it to that. */
        // TODO: Make this less stupid.
        if (msg->hdr.op < 0x11) {
            UCG2_REQUEST * match = UCG2_GetOldestRequestByOp(rqtbl, 0);
            if (!match) return empty;
        }
    }
}

/**
 * @brief Check to see if a request is not empty.
 * 
 * @param rq request to check
 * @return true request is not empty
 * @return false request is empty
 */
bool UCG2_CheckRequest(UCG2_REQUEST * rq)
{
    return (rq->target != 0) && (rq->subtarget != 0) && (rq->op != NOP);
}
/**
 * @brief Get the oldest request in the table that is valid and matches the operation.
 * 
 * @param rqtbl request table to scan
 * @param op operation to match, if 0 will match all.
 * @return UCG2_REQUEST* pointer to oldest thing that matches, or NULL.
 */
UCG2_REQUEST * UCG2_GetOldestRequestByOp(UCG2_REQUEST * rqtbl, uint8_t op)
{
    UCG2_REQUEST * best = rqtbl; // Note that this is actually the pointer to the first element.
    if (op == 0) {
        for (int i=1; i<UCG2_REQUESTBUF_LEN; i++) {
            if (UCG2_CheckRequest(&rqtbl[i]) && rqtbl[i].sent_time < best->sent_time && rqtbl[i].op == op) {
                best = &rqtbl[i];
            }
        }
    } else {
        for (int i=1; i<UCG2_REQUESTBUF_LEN; i++) {
                if (UCG2_CheckRequest(&rqtbl[i]) && rqtbl[i].sent_time < best->sent_time) {
                best = &rqtbl[i];
            }
        }
    }
    // Check to make sure the operation actually matches.  This is to stop us from being stupid.
    if (best->op != op) return NULL;
    return best;
}
/**
 * @brief Make a message with some of the arguments provided, and then send it.
 * 
 * @param inst UCG instance
 * @param target target device for message 
 * @param subtarget subtarget device for message
 * @param op operation/flag to run with message
 * @param reg register/subroutine to access
 * @param value value for payload to contain
 * @return int nonzero on error
 */
int UCG2_MakeSendMessage(UCG2_INSTANCE * inst, 
                                     uint8_t target,
                                     uint8_t subtarget,
                                     uint8_t op,
                                     uint8_t reg,
                                     UCG2_GENERIC_TYPE value)
{
    UCG2_MESSAGE resp;
    resp.hdr.source = inst->address;
    resp.hdr.source_sub = inst->subaddress;
    resp.hdr.target = target;
    resp.hdr.subtarget = subtarget;
    resp.hdr.op = op;
    resp.hdr.length = value.value_size + 1;
    resp.pl = malloc(value.value_size + 1);
    resp.pl[0] = reg;
    memcpy(&(resp.pl[1]), value.value, value.value_size);
    // Done with the value, so free it.
    if (value.freeable) free(value.value);
    UCG2NP_SendMessage(&resp);
    free(resp.pl);
    return 0;
}
/**
 * @brief Handle the contents of a single incoming message.
 * 
 * @param inst UCG instance
 * @param bytes pointer to beginning of message.  Must be a complete message.
 * @return int zero on sucessful message handle, nonzero on error handling or parsing.
 */
int UCG2_HandleSingleMessage(UCG2_INSTANCE * inst, uint8_t * bytes)
{
    UCG2_GENERIC_TYPE empty = {
        .value_size = 0,
    };
    uint8_t reg, sr;
    UCG2_REQUEST * rq;
    /* Do a couple halfhearted checks to see if bytes actually points to
    an actual message. */
    if (!UCG2_HeaderCheckValidity(bytes)) return -1;
    /* We are assuming that this is a whole message, since the NP driver 
    should have made sure it recvd the whole thing. */
    uint8_t pl_len = UCG2_HeaderGetPayloadLength(bytes);
    UCG2_MESSAGE msg = *(UCG2_MESSAGE *)bytes;
    msg.pl = &bytes[4];
    // Do stuff depending on what kind of message it was.
    switch (msg.hdr.op) {
        // Read/Request Messages
        case RQRY:
            // Register query: send the value of the register, or FAIL if it doesn't exist.
            asm("nop");
            reg = msg.pl[0];
            UCG2_GENERIC_TYPE value = UCG2_GetRegisterValueById(reg);
            if (value.value_size) {
                // Nonzero value size: register exists.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, RVAL, reg, value);
            } else {
                // Zero value size: register does not exist or something's wrong.
                UCG2_MESSAGE resp;
                resp.hdr.source = inst->address;
                resp.hdr.source_sub = inst->subaddress;
                resp.hdr.target = msg.hdr.source;
                resp.hdr.subtarget = msg.hdr.source_sub;
                resp.hdr.op = FAIL;
                resp.hdr.length = 1;
                resp.pl = &reg;
                UCG2NP_SendMessage(&resp);
                return 1;
            }
        break;
        case RTYP:
            // Register type: send the value of the register type code.
            asm("nop");
            reg = msg.pl[0];
            uint8_t reg_type;
            UCG2_REGISTER * reg_ptr = UCG2_GetRegisterById(reg);
            if (reg_ptr) {
                // Register exists, and by definition must have a type code.
                UCG2_GENERIC_TYPE typecode = {
                    .value = &(reg_ptr->type),
                    .value_size = 1
                };
                return UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, RRTC, reg, typecode);
            } else {
                // Register doesn't exist.
                UCG2_GENERIC_TYPE empty = {
                    .value_size = 0,
                };
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, reg, empty);
            }
        break;
        case SQST:
            // TODO: This feature isn't implemented yet.
            asm("nop");
            UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, NSUP, 0, empty);
        break;
        case SVAL:
            asm("nop");
            sr = msg.pl[0];
            UCG2_GENERIC_TYPE ret_val = UCG2_GetSubroutineReturnValueById(sr);
            if (ret_val.value_size) {
                // Valid subroutine.
                return UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, SRET, sr, ret_val);
            } else {
                // Something went wrong.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, sr, ret_val);
                return 1;
            }
        break;
        // Write/Run Messages
        case RWRT:
            asm("nop");
            reg = msg.pl[0];
            // Try to write to the register.
            UCG2_GENERIC_TYPE contents = {
                .value = &(msg.pl[1]),
                .value_size = msg.hdr.length
            };
            UCG2_REGISTER * regptr = UCG2_WriteRegisterValueById(reg, contents);
            if (regptr) {
                // Register write went through!
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, OPOK, reg, empty);
                return 0;
            } else {
                // Something went wrong!
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, reg, empty);
                return 1;
            }
        break;
        case SRUN:
            asm("nop");
            sr = msg.pl[0];
            // Try to start the subroutine.
            UCG2_GENERIC_TYPE arg = {
                .value = &(msg.pl[1]),
                .value_size = msg.hdr.length,
            };
            UCG2_SUBROUTINE * sr_ptr = UCG2_CallSubroutineById(sr, arg);
            if (sr_ptr) {
                // Subroutine call was successful.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, OPOK, sr, empty);
                return 0;
            } else {
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, sr, empty);
                return 1;
            }
        break;
        case STOP:
            asm("nop");
            // TODO: Actually implement this.
            UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, NSUP, 0, empty);
        break;
        // Messages that are typically responses.
        case RVAL:
            asm("nop");
            /* Find the oldest response that was an RQRY.
                If there isn't one, then FAIL this. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, RQRY);
            if (rq) {
                // Found one. Take care of the future.
                rq->fut->value.value_size = msg.hdr.length - 1;
                rq->fut->value.value = malloc(msg.hdr.length - 1);
                rq->fut->value.freeable = true;
                memcpy(rq->fut->value.value, &(msg.pl[1]), msg.hdr.length);
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // I didn't ask for this!  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        case RRTC:
            asm("nop");
            /* Find the oldest RTYP request we made.
            if there isn't one, then FAIL this. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, RTYP);
            if (rq) {
                // Found one.  Take care of the future.
                rq->fut->value.value_size = 1;
                rq->fut->value.value = malloc(1);
                rq->fut->value.freeable = true;
                *((uint8_t *)rq->fut->value.value) = msg.pl[1];
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // Why did you send this?  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        case STAT:
            asm("nop");
            /* Find the oldest response that was a SQST.
                If there isn't one, then FAIL this. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, SQST);
            if (rq) {
                // Found one. Take care of the future.
                rq->fut->value.value_size = msg.hdr.length - 1;
                rq->fut->value.value = malloc(msg.hdr.length - 1);
                rq->fut->value.freeable = true;
                memcpy(rq->fut->value.value, &(msg.pl[1]), msg.hdr.length);
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // I didn't ask for this!  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        case SRET:
            asm("nop");
            /* Find the oldest response that was an RQRY.
                If there isn't one, then FAIL this. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, SRET);
            if (rq) {
                // Found one. Take care of the future.
                rq->fut->value.value_size = msg.hdr.length - 1;
                rq->fut->value.value = malloc(msg.hdr.length - 1);
                rq->fut->value.freeable = true;
                memcpy(rq->fut->value.value, &(msg.pl[1]), msg.hdr.length);
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // I didn't ask for this!  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        // Flag handlers
        case MACK:
        case OPOK:
            asm("nop");
            /* Assume the oldest present request got MACKed or OPOKed.
                If there isn't one, then FAIL this. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, 0);
            if (rq) {
                // Found one. Take care of the future.
                rq->fut->value.value_size = 1;
                rq->fut->value.value = malloc(1);
                rq->fut->value.freeable = true;
                *((uint8_t *)rq->fut->value.value) = 0;
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // I didn't ask for this!  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        case FAIL:
        case NSUP:
            asm("nop");
            /* Assume the oldest present request got FAILed or NSUPed.
            At some point, these two codes should be treated differently
            by the command generator. */
            rq = UCG2_GetOldestRequestByOp(inst->requestbuf, 0);
            if (rq) {
                // Found one. Take care of the future.
                rq->fut->value.value_size = 1;
                rq->fut->value.value = malloc(1);
                rq->fut->value.freeable = true;
                *((uint8_t *)rq->fut->value.value) = 1;
                rq->fut->ex = true;
                // Now remove the request.
                UCG2_RemoveRequest(inst->requestbuf, *rq);
                return 0;
            } else {
                // I didn't ask for this!  Send back a FAIL.
                UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, FAIL, 0, empty);
                return 1;
            }
        break;
        case DERR:
        case DDIE:
        case REDY:
            asm("nop");
            /* These should be used for more advanced means of keeping track
            of system state outside of this device, however there's no need for that right now.
            TODO: Actually implement this. */
            return 0;
        break;
        default:
            asm("nop");
            /* I didn't understand that command. */
            UCG2_MakeSendMessage(inst, msg.hdr.source, msg.hdr.source_sub, NSUP, 0, empty);
        break;
    }
}
