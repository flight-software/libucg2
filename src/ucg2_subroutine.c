/**
 * @file ucg2_subroutine.c
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Implementation of the subroutine server.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.  
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_subroutine.h"

/// Subroutine table head node
UCG2_SRTBL_ENTRY srtbl_head;

/**
 * @brief Get a pointer to the last entry in the subroutine table linked list.
 * 
 * @return UCG2_SRTBL_ENTRY* a pointer to the last entry in the linked list.
 */
static UCG2_SRTBL_ENTRY * UCG2_GetLastSrTblEntry(void)
{
    UCG2_SRTBL_ENTRY * curr = srtbl_head.next;
    while (curr) {
        curr = curr->next;
    }
    return curr;
}

/**
 * @brief Create and immediately expose a new subroutine.
 * 
 * @param id the ID to expose the subroutine as.
 * @param sr_callback the function to run when the subroutine is called.
 * @param returns does the subroutine return a return byte?
 * @param iterative does this function need to be called repeatedly?
 * @return UCG2_SUBROUTINE* pointer to the subroutine operated on.
 */
UCG2_SUBROUTINE * UCG2_CreateSubroutine(uint8_t id, 
                                        uint8_t (*sr_callback)(UCG2_GENERIC_TYPE),
                                        bool returns,
                                        bool iterative)
{
    if (!sr_callback) return NULL;
    UCG2_SRTBL_ENTRY * entry = malloc(sizeof(UCG2_SRTBL_ENTRY));
    (UCG2_GetLastSrTblEntry())->next = entry;
    UCG2_SUBROUTINE * sr = &(entry->sr);
    sr->id = id;
    sr->sr_callback = sr_callback;
    sr->returns = returns;
    sr->iterative = iterative;
    sr->allow_repeats = true;
    sr->alert_on_finish = false;
    sr->ret_valid = false;
    return sr;
}
/**
 * @brief Return a pointer to the subroutine with specified ID.
 * 
 * @param id the ID of the subroutine to return a pointer to.
 * @return UCG2_SUBROUTINE* a pointer to the requested subroutine.  NULL if not found.
 */
UCG2_SUBROUTINE * UCG2_GetSubroutineById(uint8_t id)
{
    UCG2_SRTBL_ENTRY * curr = srtbl_head.next;
    while (curr) {
        if (curr->sr.id == id) return &(curr->sr);
    }
    return NULL;
}
/**
 * @brief Get the return value of the provided subroutine.
 * 
 * @param sr subroutine to get return value of.
 * @return UCG2_GENERIC_TYPE return value.  Always 1 byte long.
 */
UCG2_GENERIC_TYPE UCG2_GetSubroutineReturnValue(UCG2_SUBROUTINE * sr)
{
    if (!sr || !(sr->returns) || !(sr->ret_valid)) {
        UCG2_GENERIC_TYPE bad = {.value_size = 0};
        return bad;
    } else {
        UCG2_GENERIC_TYPE good = {.value = &sr->return_val, .value_size = 1};
        return good;
    }
}
/**
 * @brief Get the return value of the provided subroutine.
 * 
 * @param id ID of subroutine to get return value of.
 * @return UCG2_GENERIC_TYPE return value.  Always 1 byte long.
 */
UCG2_GENERIC_TYPE UCG2_GetSubroutineReturnValueById(uint8_t id)
{
    return UCG2_GetSubroutineReturnValue(UCG2_GetSubroutineById(id));
}
/**
 * @brief Call the provided subroutine.
 * 
 * @param sr pointer to the subroutine to call.
 * @param arg optional argument to subroutine.
 * @return UCG2_SUBROUTINE* a pointer to the subroutine operated on.  NULL on error.
 */
UCG2_SUBROUTINE * UCG2_CallSubroutine(UCG2_SUBROUTINE * sr, UCG2_GENERIC_TYPE arg)
{
    if (!sr || !sr->sr_callback) return NULL;
    if (sr->returns) {
        sr->return_val = sr->sr_callback(arg);
    } else {
        sr->sr_callback(arg);
    }
}
/**
 * @brief Call the provided subroutine, given by ID.
 * 
 * @param id ID of the subroutine to call.
 * @param arg optional argument to subroutine.
 * @return UCG2_SUBROUTINE* a pointer to the subroutine operated on.  NULL on error.
 */
UCG2_SUBROUTINE * UCG2_CallSubroutineById(uint8_t id, UCG2_GENERIC_TYPE arg)
{
    return UCG2_CallSubroutine(UCG2_GetSubroutineById(id), arg);
}
/**
 * @brief Configure various boolean subroutine options.
 * 
 * @param sr pointer to subroutine to configure
 * @param allow_repeats allow this subroutine to be run immediately after it finishes.
 * @param alert_on_finish send an OPOK flag after this subroutine finishes.
 * @return UCG2_SUBROUTINE* a pointer to the subroutine operated on.  NULL on error.
 */
UCG2_SUBROUTINE * UCG2_ConfigureSubroutine(UCG2_SUBROUTINE * sr, bool allow_repeats, bool alert_on_finish)
{
    if (!sr) return NULL;
    // Configure the booleans.  No checking needs to be performed.
    sr->allow_repeats = allow_repeats;
    sr->alert_on_finish = alert_on_finish;
    return sr;
}
/**
 * @brief Configure various boolean subroutine options.
 * 
 * @param id ID of subroutine to operate on.
 * @param allow_repeats allow this subroutine to be run immediately after it finishes.
 * @param alert_on_finish send an OPOK flag after this subroutine finishes.
 * @return UCG2_SUBROUTINE* a pointer to the subroutine operated on.  NULL on error.
 */
UCG2_SUBROUTINE * UCG2_ConfigureSubroutineById(uint8_t id, bool allow_repeats, bool alert_on_finish)
{
    return UCG2_ConfigureSubroutine(UCG2_GetSubroutineById(id), allow_repeats, alert_on_finish);
}
/**
 * @brief Destroy and free an existing subroutine.
 * 
 * @param id ID of the subroutine to destroy.
 * 
 * Pointers to the subroutine or any of its elements will be unusable after this function is called.
 */
void UCG2_DestroySubroutine(uint8_t id)
{
    UCG2_SRTBL_ENTRY * entry, * curr, * last;
    curr = srtbl_head.next;
    last = &srtbl_head;
    while (curr) {
        if (curr->sr.id == id) {
            goto found;
        }
        curr = curr->next;
        last = curr;
    }
    return;
    found:
    asm("nop");
    UCG2_SUBROUTINE * sr = &(entry->sr);
    // Redo link
    last->next = curr->next;
    free(curr);
}
/**
 * @brief Free all subroutines in the subroutine table.
 * 
 */
void UCG2_DestroyAllSubroutines()
{
    UCG2_SRTBL_ENTRY * curr = srtbl_head.next;
    UCG2_SRTBL_ENTRY * next;
    while (curr) {
        next = curr->next;
        free(curr);
    }
}