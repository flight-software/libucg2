/**
 * @file ucg2_register.c
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Implementation of the register server model parser.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_register.h"

UCG2_REGTBL_ENTRY regtbl_head = {.next=NULL};

/**
 * @brief Get a pointer to the last entry in the register table linked list.
 * 
 * @return UCG2_REGTBL_ENTRY* a pointer to the last entry in the linked list.
 */
static UCG2_REGTBL_ENTRY * UCG2_GetLastRegTblEntry(void)
{
    UCG2_REGTBL_ENTRY * curr = &regtbl_head;
    while (curr->next) {
        curr = curr->next;
    }
    return curr;
}

/**
 * @brief Create and immediately expose a new register.
 * 
 * @param id the ID to expose the register as.
 * @param type the UCG type of the register's data.
 * @param size the size in bytes that the register should be.
 * @param allocate if true, a byte array of the specified length will be allocated automatically.
 * @return UCG2_REGISTER* a pointer to the newly created register.  This is already in the register table.
 */
UCG2_REGISTER * UCG2_CreateRegister(uint8_t id, UCG2_REGTYPE type, uint8_t size, bool allocate)
{
    // Basic linked-list entry kind of stuff.  Malloc a new one, put it in the list.
    // The entry holds the data of the register, not a pointer to it, so it is also allocated.
    UCG2_REGTBL_ENTRY * entry = malloc(sizeof(UCG2_REGTBL_ENTRY));
    (UCG2_GetLastRegTblEntry())->next = entry;
    UCG2_REGISTER * reg = &(entry->reg);
    entry->next = 0x0;
    reg->id = id;
    reg->type = type;
    if (size <= 63) {
        reg->size = size;
    }
    else {
        // Clamp the size to 63 bytes.
        reg->size = 63;
    }
    if (allocate) {
        reg->value = malloc(size);
        reg->freeable = true;
        reg->writeable = true;
        reg->readable = true; // The data won't be good, but it's not illegal to read.
    }
    else {
        reg->value = NULL;
        reg->freeable = false;
        // Because there's no memory and no callbacks yet, default to not readable or writeable.
        reg->readable = false;
        reg->writeable = false;
    }
    reg->cached = false;
    reg->computed = false;
    // It'll still segfault if you do something wrong but this is easier to check for blank-ness.
    reg->reg_callback = NULL;
    reg->reg_wrt_callback = NULL;
}
/**
 * @brief Return a pointer to the register with a specified ID.
 * 
 * @param id the ID of the register to return a pointer to.
 * @return UCG2_REGISTER* a pointer to the requested register.  NULL if not found.
 */
UCG2_REGISTER * UCG2_GetRegisterById(uint8_t id)
{
    UCG2_REGTBL_ENTRY * curr = regtbl_head.next;
    while (curr) {
        if (curr->reg.id == id) return &(curr->reg);
        curr = curr->next;
    }
    return NULL;
}
/**
 * @brief Forcibly set the mode booleans of the register.
 * 
 * @param reg pointer to the register to operate on.
 * @param computed is this register computed (uses callbacks)?
 * @param readable is this register readable?
 * @param writeable can this register be written to?
 * @param strict should the handler reject improperly sized writes?
 * @param cached should the calculated value of this register be cached?
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This function will reject improperly formed combinations of inputs.  It will correct or reject the following conditions:
 * * Attempting to make a variable-based register readable or writable when the value pointer is NULL.
 * * Attempting to make a calculated register readable or writeable when the respective callbacks are NULL.
 * * Setting 'computed' to false when the register is read/write and the value pointer is NULL.
 * * Attempting to enable caching when the value pointer is NULL.
 * This function prefers to silently fix invalid inputs rather than rejecting the entire function call.
 */
UCG2_REGISTER * UCG2_SetMode(UCG2_REGISTER * reg, bool computed, bool readable, bool writeable, bool strict, bool cached)
{
    if (!reg) return NULL;
    bool readwrite_lock = false;
    // Set the value of 'computed'
    // Catch conditions 1 and 3
    if (!computed && !(reg->value)) {
        reg->readable = false;
        reg->writeable = false;
        readwrite_lock = true; // Set this so we don't override this later.
    }
    reg->computed = computed;
    // Set read/write
    if (!readwrite_lock) {
        // Attempt to catch condition 2.
        if (computed) {
            if (readable) {
                if (reg->reg_callback) {
                    reg->readable = true;
                } else {
                    // Violates condition 2.
                    reg->readable = false;
                }
            } else {
                reg->readable = false;
            }
            if (writeable) {
                if (reg->reg_wrt_callback) {
                    reg->writeable = true;
                } else {
                    // Violates condition 2.
                    reg->writeable = false;
                }
            } else {
                reg->readable = false;
            }
        } else {
            reg->readable = readable;
            reg->writeable = writeable;
        }
    }
    // Set strict.  No checks for this one.
    reg->strict = strict;
    // Set cached.
    reg->cached = cached;
    // Catch condition 4.
    if (!(reg->value)) {
        cached = false;
    }
}
/**
 * @brief Forcibly set the mode booleans of the register by ID.
 * 
 * @param id 
 * @param computed is this register computed (uses callbacks)?
 * @param readable is this register readable?
 * @param writeable can this register be written to?
 * @param strict should the handler reject improperly sized writes?
 * @param cached should the calculated value of this register be cached?
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This is mostly a convenience wrapper of UCG2_SetMode.
 * For more information on the inner machinations of this function, see the docs for UCG2_SetMode.
 */
UCG2_REGISTER * UCG2_SetModeById(uint8_t id, bool computed, bool readable, bool writeable, bool strict, bool cached)
{
    return UCG2_SetMode(UCG2_GetRegisterById(id), computed, readable, writeable, strict, cached);
}
/**
 * @brief Set the callback functions for a computed register.
 * 
 * @param reg register to operate on.
 * @param reg_callback function which returns a UCG2_REG_CB_RET_TYPE struct, called when register is read.
 * @param reg_wrt_callback function which accepts a UCG2_REG_CB_ARG_TYPE struct, called when register is written.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * These arguments can be NULL, if it is desired to set them to be that way.
 */
UCG2_REGISTER * UCG2_SetRegisterCallbacks(UCG2_REGISTER * reg, UCG2_REG_CB_RET_TYPE (*reg_callback)(void),
                                            int (*reg_wrt_callback)(UCG2_REG_CB_ARG_TYPE))
{
    if (!reg) return NULL;
    reg->reg_callback = reg_callback;
    reg->reg_wrt_callback = reg_wrt_callback;
    return reg;
}
/**
 * @brief Set the callback functions for a computed register, given by ID.
 * 
 * @param id ID of register to operate on.
 * @param reg_callback function which returns a UCG2_REG_CB_RET_TYPE struct, called when register is read.
 * @param reg_wrt_callback function which accepts a UCG2_REG_CB_ARG_TYPE struct, called when register is written.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * These arguments can be NULL, if it is desired to set them to be that way.
 */
UCG2_REGISTER * UCG2_SetRegisterCallbacksById(uint8_t id, UCG2_REG_CB_RET_TYPE (*reg_callback)(void),
                                                int (*reg_wrt_callback)(UCG2_REG_CB_ARG_TYPE))
{
    return UCG2_SetRegisterCallbacks(UCG2_GetRegisterById(id), reg_callback, reg_wrt_callback);
}
/**
 * @brief Enable or disable caching of values for a given register.
 * 
 * @param reg pointer to the register to operate on.
 * @param cached desired cache mode, on or off.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This function enforces condition 4 of UCG2_SetMode, namely that caching cannot be enabled if 
 * the value pointer of the register is NULL.
 */
UCG2_REGISTER * UCG2_ChangeCachedMode(UCG2_REGISTER * reg, bool cached)
{
    if (!reg) return NULL;
    // Enforce condition 4 while setting the value.
    reg->cached = (reg->value && cached);
    // See?  It was that simple.
    return reg;
}
/**
 * @brief Enable or disable caching of values for a given register.
 * 
 * @param id ID of register to operate on.
 * @param cached desired cache mode, on or off.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This function enforces condition 4 of UCG2_SetMode, namely that caching cannot be enabled if 
 * the value pointer of the register is NULL.
 */
UCG2_REGISTER * UCG2_ChangeCachedModeById(uint8_t id, bool cached)
{
    return UCG2_ChangeCachedMode(UCG2_GetRegisterById(id), cached);
}
/**
 * @brief For a cached register, set the maximum time before a refresh.
 * 
 * @param reg pointer to the register to operate on.
 * @param timeout timeout in ms before a refresh is required.
 * @return UCG2_REGISTER* pointer to the register that was just operated on. NULL on error.
 * 
 * This function is dumb and will work whether or not you actually have caching enbaled.
 */
UCG2_REGISTER * UCG2_SetCacheTimeout(UCG2_REGISTER * reg, uint16_t timeout)
{
    if (!reg) return NULL;
    reg->cache_timeout = timeout;
    return reg;
}
/**
 * @brief For a cached register, set the maximum time before a refresh.
 * 
 * @param id ID of register to operate on.
 * @param timeout timeout in ms before a refresh is required.
 * @return UCG2_REGISTER* pointer to the register that was just operated on. NULL on error.
 * 
 * This function is dumb and will work whether or not you actually have caching enbaled.
 */
UCG2_REGISTER * UCG2_SetCacheTimeoutById(uint8_t id, uint16_t timeout)
{
    return UCG2_SetCacheTimeout(UCG2_GetRegisterById(id), timeout);
}
/**
 * @brief Resize an existing register.
 * 
 * @param id ID of register to operate on.
 * @param size new size of register.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * THIS IS VERY UNSAFE IF YOU HAVEN'T CHANGED THE ASSIGNED VARIABLE!
 * The size can be zero, but if set to zero, the existing size will remain 
 * but the register will be set to unreadable and unwriteable.
 */
UCG2_REGISTER * UCG2_ResizeRegisterById(uint8_t id, uint8_t size)
{
    UCG2_REGISTER * reg = UCG2_GetRegisterById(id);
    if (!reg) return NULL;
    if (size == 0) {
        // Don't actually change the size, instead make it unreadable and unwriteable.
        reg->readable = 0;
        reg->writeable = 0;
        return reg;
    } else {
        reg->size = size;
    }
}
/**
 * @brief Destroy and free an existing register.
 * 
 * @param id ID of the register to destroy.
 * 
 * Pointers to the register or any of its elements will be unusable after this function is called.
 * If the register was created with the 'allocate' option set, the value field will also be freed.
 */
void UCG2_DestroyRegister(uint8_t id)
{
    UCG2_REGTBL_ENTRY * entry, * curr, * last;
    curr = regtbl_head.next;
    last = &regtbl_head;
    while (curr) {
        if (curr->reg.id == id) {
            goto found;
        }
        curr = curr->next;
        last = curr;
    }
    return;
    found:
    asm("nop");
    UCG2_REGISTER * reg = &(entry->reg);
    if (!reg) return; // This is highly unlikely.
    if (reg->freeable && reg->value) {
        free(reg->value);
    }
    // Redo link
    last->next = curr->next;
    free(curr);
}
/**
 * @brief Free all registers in the register table.
 * 
 */
void UCG2_DestroyAllRegisters()
{
    UCG2_REGTBL_ENTRY * curr = regtbl_head.next;
    UCG2_REGTBL_ENTRY * next;
    while (curr) {
        next = curr->next;
        if (curr->reg.freeable && curr->reg.value) {
            free(curr->reg.value);
        }
        free(curr);
    }
}
/**
 * @brief Set the value of a register specified by ID.
 * 
 * @param id ID of register to operate on. 
 * @param value value to assign to the register.
 * @param size size of the value argument.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error. 
 */
UCG2_REGISTER * UCG2_SetRegisterValueById(uint8_t id, void * value, uint8_t size)
{
    UCG2_REGISTER * reg = UCG2_GetRegisterById(id);
    if (!reg || !reg->writeable) return NULL;
    // Enforce strict mode.
    if ((size != reg->size) && reg->strict) return NULL;
    // Clamp size to register size.
    uint8_t mysize = (size > reg->size)? reg->size : size;
    // Copy value byte-wise into the register.
    for (int i=0; i<mysize; i++) {
        ((uint8_t*)(reg->value))[i] = ((uint8_t*)value)[i];
    }
}
/**
 * @brief Write to a local register, taking into account computed registers.
 * 
 * @param id ID of register to operate on.
 * @param value value and size to assign to the register
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 */
UCG2_REGISTER * UCG2_WriteRegisterValueById(uint8_t id, UCG2_GENERIC_TYPE value)
{
    UCG2_REGISTER * reg = UCG2_GetRegisterById(id);
    if (!reg || !reg->writeable) return NULL;
    // Enforce strict mode
    if ((value.value_size != reg->size) && reg->strict) return NULL;
    // Branch depending on if this is a calculated register
    if (reg->computed && reg->reg_wrt_callback) {
        // This is a valid calculated register.
        reg->reg_wrt_callback(value);
        return reg;
    } else if (reg->value) {
        // This is a valid memory register.
        if (UCG2_SetRegisterValueById(id, value.value, value.value_size)) return reg;
        else return NULL;
    } else {
        return NULL;
    }
}
/**
 * @brief Get the value of a register specified by ID.
 * 
 * @param id ID of register to operate on.
 * @return UCG2_GENERIC_TYPE Value of the register, paired with its size.  0 size on error.
 */
UCG2_GENERIC_TYPE UCG2_GetRegisterValueById(uint8_t id)
{
    UCG2_REGISTER * reg = UCG2_GetRegisterById(id);
    UCG2_GENERIC_TYPE ret_val;
    if (!reg) {
        ret_val.value_size = 0;
        return ret_val;
    }
    if (reg->computed) {
        if (reg->cached) {
            // Use implementation-defined things to determine if the value is timed out.
            if (UCG2NP_GetAbsTime() <= (reg->computed_time + reg->cache_timeout)) {
                // Use the cached value.
                ret_val.value = reg->value;
                ret_val.value_size = reg->size;
                return ret_val;
            }
            // Cache is timed out, so recompute and save.
        }
        // Recompute the value
        UCG2_GENERIC_TYPE temp = reg->reg_callback();
        if ((reg->cached && !reg->strict) || (reg->cached && (reg->size == temp.value_size)) && reg->value) {
            // This means that it's cached and if it's strict, the size is right. Also the value is not a null pointer.
            // Write the value to cache.
            reg->size = temp.value_size;
            memcpy(reg->value, temp.value, temp.value_size);
        }
        return temp;
    } else {
        ret_val.value = reg->value;
        ret_val.value_size = reg->size; // We assume the value is the same size as the register...
        return ret_val;
    }
}
/**
 * @brief Assign a pointer to an existing variable as the value field for the specified register.
 * 
 * @param reg pointer to register to operate on.
 * @param valueptr pointer to variable to assign to register.
 * @param size size of assigned variable in bytes.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This function will reject any value with a specified size greater than 63 bytes.
 * It is acceptable to have valueptr be NULL, in which case it will be set unreadable
 * and unwriteable until instructed otherwise.
 */
UCG2_REGISTER * UCG2_AssignVariable(UCG2_REGISTER * reg, void * valueptr, uint8_t size)
{
    if (!reg) return NULL;
    if (size > 63) return NULL;
    reg->value = valueptr;
    uint8_t mysize = (!valueptr)? 0 : size;
    reg->size = mysize;
    if (valueptr) {
        reg->readable = true;
        reg->writeable = true;
    } else {
        reg->readable = false;
        reg->writeable = false;
    }
    return reg;
}
/**
 * @brief Assign a pointer to an existing variable as the value field for the specified register.
 * 
 * @param id ID of register to operate on.
 * @param valueptr pointer to variable to assign to register.
 * @param size size of assigned variable in bytes.
 * @return UCG2_REGISTER* pointer to the register that was just operated on.  NULL on error.
 * 
 * This function will reject any value with a specified size greater than 63 bytes.
 * It is acceptable to have valueptr be NULL, in which case it will be set unreadable
 * and unwriteable until instructed otherwise.
 */
UCG2_REGISTER * UCG2_AssignVariableById(uint8_t id, void * valueptr, uint8_t size)
{
    return UCG2_AssignVariable(UCG2_GetRegisterById(id), valueptr, size);
}