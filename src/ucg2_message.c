/**
 * @file ucg2_message.c
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Basic message handling capabilities.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.  
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_message.h"

/**
 * @brief Attempt to check the validity of this message header.
 * 
 * @param header pointer to the message header.  Must have >=3 bytes that are valid.
 * @return true header is probably valid.
 * @return false header is probably invalid.
 */
bool UCG2_HeaderCheckValidity(uint8_t * header)
{
    // Pretend this pointer points to a header.
    UCG2_HEADER * hdr = (UCG2_HEADER *)header;
    // Check to see if the op is a valid number for the things currently implemented.
    if (hdr->op < 0x1 || hdr->op > 0xB) return false;
    if (!hdr->source) return false;
    return true;
}

/**
 * @brief Verify and get the expected payload length of the provided header.
 * 
 * @param header pointer to the message header.  Must have >=3 bytes that are valid.
 * @return uint8_t expected payload length.  Will be invalid (>63) on error.
 */
uint8_t UCG2_HeaderGetPayloadLength(uint8_t * header)
{
    // Check to see if this is a valid header.
    if (!UCG2_HeaderCheckValidity(header)) return 64;
    // Return the payload length.
    return ((UCG2_HEADER*)header)->length;
}