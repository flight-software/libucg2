/**
 * @file ucg2_pseudofutures.h
 * @author Bad implementation of a future type.
 * @brief 
 * @version 0.1
 * @date 2019-07-11
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_pseudofutures.h"

/**
 * @brief Wait for the future to be realized, and then return the value.
 * 
 * @param fut future to poll for
 * @return UCG2_GENERIC_TYPE* value of the future.  NULL/0 on failure or timeout.
 */
UCG2_GENERIC_TYPE UCG2_PollForFuture(UCG2_FUTURE_TIMEOUT * fut)
{
    // Make a blank element in case of failure
    UCG2_GENERIC_TYPE bad = {.value = NULL, .value_size = 0};
    // Poll the future.
    while (!(fut->ex)) {
        // Check to see if it has timed out
        if (UCG2NP_GetAbsTime() > fut->timeout + fut->created_ms) return bad;
        asm("nop");
    }
    // Return the value
    return fut->value;
}
/**
 * @brief Check to see if a future is done, or if it has timed out.
 * 
 * @param fut future to check on
 * @return int 0 if done, -1 if timed out, 1 if not done.
 */
int UCG2_CheckFuture(UCG2_FUTURE_TIMEOUT * fut)
{
    if (fut->ex) return 0;
    if (UCG2NP_GetAbsTime() > fut->timeout + fut->created_ms) return -1;
    return 1;
}