/**
 * @file ucg2_pseudofutures.h
 * @author Bad implementation of a future type.
 * @brief 
 * @version 0.1
 * @date 2019-07-11
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
#include "ucg2_register.h"
#include "ucg2_np.h"

/// Future with builtin timeout information.
typedef struct {
    UCG2_GENERIC_TYPE value;
    bool ex;
    uint32_t created_ms;
    uint16_t timeout;
} UCG2_FUTURE_TIMEOUT;

// Function prototypes
UCG2_GENERIC_TYPE UCG2_PollForFuture(UCG2_FUTURE_TIMEOUT * fut);
int                 UCG2_CheckFuture(UCG2_FUTURE_TIMEOUT * fut);