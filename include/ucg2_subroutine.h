/**
 * @file ucg2_subroutine.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Subroutine data structures.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.  
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
#include "ucg2_register.h"

// Subroutine server data structures.
/// Internal representation of a subroutine
typedef struct {
    uint8_t id;
    uint8_t (*sr_callback)(UCG2_GENERIC_TYPE);
    bool returns;
    uint8_t return_val;
    bool allow_repeats;
    bool alert_on_finish;
    bool iterative;
    bool ret_valid;
} UCG2_SUBROUTINE;

/// Subroutine table entry structure.
typedef struct srtbl_entry {
    UCG2_SUBROUTINE sr;
    struct srtbl_entry * next;
} UCG2_SRTBL_ENTRY;

// Global variables
extern UCG2_SRTBL_ENTRY srtbl_head;

// Function prototypes
UCG2_SUBROUTINE * UCG2_CreateSubroutine(uint8_t id, 
                                        uint8_t (*sr_callback)(UCG2_GENERIC_TYPE),
                                        bool returns,
                                        bool iterative);
UCG2_SUBROUTINE * UCG2_GetSubroutineById(uint8_t id);
UCG2_GENERIC_TYPE UCG2_GetSubroutineReturnValue(UCG2_SUBROUTINE * sr);
UCG2_GENERIC_TYPE UCG2_GetSubroutineReturnValueById(uint8_t id);
UCG2_SUBROUTINE * UCG2_CallSubroutine(UCG2_SUBROUTINE * sr, UCG2_GENERIC_TYPE arg);
UCG2_SUBROUTINE * UCG2_CallSubroutineById(uint8_t id, UCG2_GENERIC_TYPE arg);
UCG2_SUBROUTINE * UCG2_ConfigureSubroutine(UCG2_SUBROUTINE * sr, bool allow_repeats, bool alert_on_finish);
UCG2_SUBROUTINE * UCG2_ConfigureSubroutineById(uint8_t id, bool allow_repeats, bool alert_on_finish);
void UCG2_DestroySubroutine(uint8_t id);
void UCG2_DestroyAllSubroutines(void);
