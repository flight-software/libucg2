/**
 * @file ucg2_register.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Data structures and function definitions for the register server model parser.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "ucg2_np.h"

// Register typing information
/// Register types
typedef enum {
    RTYPE_ERROR=-1,
    RTYPE_UNTYPED=0,
    RTYPE_BYTES,
    RTYPE_INT,
    RTYPE_UINT,
    RTYPE_FLOAT,
    RTYPE_DOUBLE,
    RTYPE_BOOL,
    RTYPE_ARRAY,
    RTYPE_UCGTIME
} UCG2_REGTYPE;

// Register server data structures.
typedef struct {
    uint8_t value_size;
    void * value;
    bool freeable;
} UCG2_REG_CB_RET_TYPE;
typedef UCG2_REG_CB_RET_TYPE UCG2_REG_CB_ARG_TYPE;
typedef UCG2_REG_CB_RET_TYPE UCG2_GENERIC_TYPE;
/// Internal representation of a register.
typedef struct {
    uint8_t id; ///< Register ID that is exposed to the bus.
    uint8_t size; ///<  Size of the register in bytes.
    UCG2_REGTYPE type; ///< Type of data the register is intended to hold.
    bool computed; ///< Is the value of this register computed when requested?
    void * value; ///< Pointer to the value of the register, for a stored-value register.
    bool freeable; ///< Should the thing this pointer points to be freed on destruction?
    UCG2_REG_CB_RET_TYPE (*reg_callback)(void); ///< If this is a computed register, the function to call.
    bool readable; ///< Is this register readable?
    bool writeable; ///< Is this register writeable?
    int (*reg_wrt_callback)(UCG2_REG_CB_ARG_TYPE); ///< If this is a computed register and it is written to, the function to call.
    bool strict; ///< Enable strict mode, where writes must be the exact right size to be valid and cannot be padded.
    bool cached; ///< Enable caching of computed values.  This requires both a callback and a variable pointer to be present.
    uint16_t cache_timeout; ///< Time in ms that a cached value is valid.
    uint32_t computed_time; ///< Some time in ms that the cached value was computed at.
} UCG2_REGISTER;

/// Register table entry structure.  Just a typical linked-list structure.
typedef struct regtbl_entry {
    UCG2_REGISTER reg;
    struct regtbl_entry * next;
} UCG2_REGTBL_ENTRY;

// Global variables
extern UCG2_REGTBL_ENTRY regtbl_head;

// Function prototypes
UCG2_REGISTER * UCG2_CreateRegister(uint8_t id, UCG2_REGTYPE type, uint8_t size, bool allocate);
UCG2_REGISTER * UCG2_GetRegisterById(uint8_t id);
UCG2_REGISTER * UCG2_SetRegisterValueById(uint8_t id, void * value, uint8_t size);
UCG2_REGISTER * UCG2_WriteRegisterValueById(uint8_t id, UCG2_GENERIC_TYPE value);
UCG2_GENERIC_TYPE UCG2_GetRegisterValueById(uint8_t id);
UCG2_REGISTER * UCG2_AssignVariable(UCG2_REGISTER * reg, void * valueptr, uint8_t size);
UCG2_REGISTER * UCG2_AssignVariableById(uint8_t id, void * valueptr, uint8_t size);
UCG2_REGISTER * UCG2_SetMode(UCG2_REGISTER * reg, bool computed, bool readable, bool writeable, bool strict, bool cached);
UCG2_REGISTER * UCG2_SetModeById(uint8_t id, bool computed, bool readable, bool writeable, bool strict, bool cached);
UCG2_REGISTER * UCG2_SetRegisterCallbacks(UCG2_REGISTER * reg, UCG2_REG_CB_RET_TYPE (*reg_callback)(void),
                                            int (*reg_wrt_callback)(UCG2_REG_CB_ARG_TYPE));
UCG2_REGISTER * UCG2_SetRegisterCallbacksById(uint8_t id, UCG2_REG_CB_RET_TYPE (*reg_callback)(void),
                                                int (*reg_wrt_callback)(UCG2_REG_CB_ARG_TYPE));
UCG2_REGISTER * UCG2_ChangeCachedMode(UCG2_REGISTER * reg, bool cached);
UCG2_REGISTER * UCG2_ChangeCachedModeById(uint8_t id, bool cached);
UCG2_REGISTER * UCG2_SetCacheTimeout(UCG2_REGISTER * reg, uint16_t timeout);
UCG2_REGISTER * UCG2_SetCacheTimeoutById(uint8_t id, uint16_t timeout);
UCG2_REGISTER * UCG2_ResizeRegisterById(uint8_t id, uint8_t size);
void            UCG2_DestroyRegister(uint8_t id);
void            UCG2_DestroyAllRegisters(void);


