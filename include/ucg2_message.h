/**
 * @file ucg2_message.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief UCGv2 message structure and internal representations.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.  
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
#include <stdint.h>
#include <stdbool.h>

_Pragma("GCC diagnostic push")
_Pragma("GCC diagnostic ignored \"-Wpacked\"")
_Pragma("GCC diagnostic ignored \"-Wattributes\"")

// Communicated message structures.
/// UCG Message Header.  3 bytes.
typedef struct {
    unsigned int target:    5;
    unsigned int subtarget: 3;
    unsigned int source:    5;
    unsigned int source_sub:3;
    unsigned int op:        5;
    unsigned int length:    8;
    int                    :3; // We do not support long registers so ignore the upper 3 bits
}__attribute__((packed)) UCG2_HEADER;

/// Internal representation of a UCG message.
typedef struct {
    UCG2_HEADER hdr;
    uint8_t * pl;
} UCG2_MESSAGE;

/// Time field for scripted messages.
typedef struct {
    bool rel:    1;
    uint32_t t: 31;
}__attribute__((packed)) UCG2_TIME;

/// Scripted message header data representation.
typedef struct {
    UCG2_TIME tm;
    UCG2_HEADER hdr;
}__attribute__((packed)) UCG2_SCRIPTED_HEADER;

_Pragma("GCC diagnostic pop")

// Function prototypes
bool UCG2_HeaderCheckValidity(uint8_t * header);
uint8_t UCG2_HeaderGetPayloadLength(uint8_t * header);
