/**
 * @file ucg2_np.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Header that takes care of platform-specific header inclusion.
 * @version 0.1
 * @date 2019-07-11
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
// Hardware interface headers
#ifdef UCG2_LINUX_GNU
#include "ucg2_np_linux.h"
#endif /* UCG2_LINUX_GNU */
// TODO: REMOVE THE COMMENTS
#ifdef UCG2_STM32_HAL
#include "ucg2_np_stmcube.h"
#endif /* UCG2_STM32_HAL */
