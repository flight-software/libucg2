/**
 * @file ucg2_engine.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief UCG handler, runner, etc.
 * @version 0.1
 * @date 2019-07-10
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 * 
 * ### Blurb about how this works:
 * The UCG2 engine takes care of sending and receiving messages,
 * at the lowest level.  It also provides protocol-level transaction control.
 * The protocol level essentially revolves around the request buffer.
 * Because UCG provides information regarding who sent a message, and its contents,
 * a request is held in the buffer until a suitable response is received.
 * It is assumed that responses will be in the same order as the requests were sent.
 */
#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "ucg2_message.h"
#include "ucg2_np.h"
#include "ucg2_operation.h"
#include "ucg2_register.h"
#include "ucg2_subroutine.h"
#include "ucg2_pseudofutures.h"

#define UCG2_REQUESTBUF_LEN 16

extern UCG2_GENERIC_TYPE UCG2_empty_value;

/// Request data structure
typedef struct {
    unsigned int target;
    unsigned int subtarget;
    unsigned int op;
    uint8_t target_id;
    uint32_t sent_time;
    UCG2_FUTURE_TIMEOUT * fut;
} UCG2_REQUEST;

/// Inbound message data structure
typedef struct {
    unsigned int op;
    uint8_t target_id;
    uint8_t * data_bytes;
} UCG2_INBOUND;

/// Instance data structure
typedef struct {
    uint8_t address;
    uint8_t subaddress;
    uint8_t mode;
    bool enabled;
    int timeout;
    bool persistent;
    UCG2_REQUEST * requestbuf;
} UCG2_INSTANCE;


// Functions to define
// Top-level request functions
int UCG2_GetRegisterType(UCG2_INSTANCE * inst,
                        uint8_t target, 
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_FUTURE_TIMEOUT * fut);
int UCG2_ReadRegister(UCG2_INSTANCE * inst,
                        uint8_t target, 
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_FUTURE_TIMEOUT * fut);
int UCG2_GetSubroutineStatus(UCG2_INSTANCE * inst,
                            uint8_t target,
                            uint8_t subtarget,
                            uint8_t sr,
                            UCG2_FUTURE_TIMEOUT * fut);
int UCG2_GetSubroutineReturn(UCG2_INSTANCE * inst,
                                uint8_t target,
                                uint8_t subtarget,
                                uint8_t sr,
                                UCG2_FUTURE_TIMEOUT * fut);
int UCG2_WriteRegister(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t reg,
                        UCG2_GENERIC_TYPE data,
                        UCG2_FUTURE_TIMEOUT * fut);
int UCG2_RunSubroutine(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t sr,
                        UCG2_GENERIC_TYPE arg,
                        UCG2_FUTURE_TIMEOUT * fut);
int UCG2_StopSubroutine(UCG2_INSTANCE * inst,
                        uint8_t target,
                        uint8_t subtarget,
                        uint8_t sr,
                        UCG2_FUTURE_TIMEOUT * fut);
// Core Engine Functions
int UCG2_AddRequest(UCG2_REQUEST * rqtbl, UCG2_REQUEST rq);
int UCG2_AddRequestFromMessage(UCG2_REQUEST * rqtbl, UCG2_MESSAGE * msg, UCG2_FUTURE_TIMEOUT * fut);
int UCG2_RemoveRequest(UCG2_REQUEST * rqtbl, UCG2_REQUEST rq_match);
UCG2_REQUEST UCG2_GenerateRequestFromMessage(UCG2_MESSAGE * msg, UCG2_FUTURE_TIMEOUT * fut);
UCG2_REQUEST UCG2_GenerateRequestMatch(UCG2_REQUEST * rqtbl, UCG2_MESSAGE * msg);
int UCG2_HandleSingleMessage(UCG2_INSTANCE * inst, uint8_t * bytes);
UCG2_REQUEST * UCG2_GetOldestRequestByOp(UCG2_REQUEST * rqtbl, uint8_t op);
int UCG2_MakeSendMessage(UCG2_INSTANCE * inst, 
                                     uint8_t target,
                                     uint8_t subtarget,
                                     uint8_t op,
                                     uint8_t reg,
                                     UCG2_GENERIC_TYPE value);
