/**
 * @file ucg2_operation.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Operation code definitions.
 * @version 0.1
 * @date 2019-07-09
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
/// Valid builtin operations
typedef enum {
    NOP=0,
    RQRY=1,
    SQST=2,
    SVAL=3,
    RTYP=4,
    RVAL=5,
    RWRT=6,
    RRTC=7,
    SRUN=8,
    STAT=9,
    STOP=0xA,
    SRET=0xB
} UCG2_OP;

/// Valid builtin flags
typedef enum {
    NFLG=0x0,
    MACK=0xD,
    OPOK=0xE,
    FAIL=0xF,
    NSUP=0x10,
    DERR=0x11,
    DDIE=0x12,
    REDY=0x13,
} UCG2_FLAG;