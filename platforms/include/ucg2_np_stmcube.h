/**
 * @file ucg2_np_stmcube.h
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Device-specific functions for STM32 using STM32Cube.
 * @version 0.1
 * @date 2019-07-11
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#pragma once
// Files to include
#include <stdint.h>
#ifdef STM32G0
#include "stm32g0xx.h"
#endif /* STM32G0 */
#ifdef STM32L4
#include "stm32l4xx.h"
#endif /* STM32L4 */
#include "ucg2_message.h"

extern UART_HandleTypeDef * UCG2_NP_STM32_huart;

// Functions to implement
uint32_t UCG2NP_GetAbsTime(void);
uint8_t UCG2NP_SendMessage(UCG2_MESSAGE * msg);
