/**
 * @file ucg2_np_stmcube.c
 * @author Logan Power (lmpower2@illinois.edu)
 * @brief Device-specific functions for STM32 using STM32Cube.
 * @version 0.1
 * @date 2019-07-11
 * 
 * @copyright Copyright (c) 2019 Laboratory for Advanced Space Systems at Illinois.
 * @license Released under the terms of the University of Illinois/NCSA License found in LICENSE.txt
 */
#include "ucg2_np_stmcube.h"

UART_HandleTypeDef * UCG2_NP_STM32_huart;

uint32_t UCG2NP_GetAbsTime(void)
{
    return HAL_GetTick();
}

uint8_t UCG2NP_SendMessage(UCG2_MESSAGE * msg)
{
    uint8_t errorvar = 0;
    errorvar |= HAL_UART_Transmit_DMA(UCG2_NP_STM32_huart, &(msg->hdr), 4);
    errorvar |= HAL_UART_Transmit_DMA(UCG2_NP_STM32_huart, msg->pl, msg->hdr.length);
    return errorvar;
}